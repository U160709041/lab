public class CircleDemo{

	public static void main(String[] args){
		
		int radius = 5;
		double area = (3.14)*(radius*radius);
		System.out.println(area);

		double circumference = (3.14)* 2*radius;
		System.out.println(circumference);
	}
}

