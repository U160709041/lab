public class Questionsix{

	public static int function(int n){
		if(n == 0)
			return 0;
		else if( n == 1)
			return 1;
		else 
			return function(n-1) + 2* function(n-2);
	}
}

//output f(6) = 21
