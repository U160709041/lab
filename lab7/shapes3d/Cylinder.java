package ceyda.shapes3d;

import ceyda.shapes.Circle;

public class Cylinder extends Circle{

	int height;
	
	public Cylinder(int radius, int height){
		super(radius);
		this.height = height;
	}

	public double area(){
		return 2 * super.area() + 2 * 3.14 * getRadius();
	}

	public double volume(){
		return super.area() * height;
	}

	public String toString(){
		return "radius: " + getRadius() + "height" + height;
	}
}
